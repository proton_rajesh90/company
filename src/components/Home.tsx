/**
 * Author: flegile.com
 */

import * as React from 'react';
import { Container } from './Container';
import { Contact } from './Contact';
import { About } from './About';

interface IHomeState {
  scrollY: number;
  isAboutVisible: boolean;
  isContactVisisble: boolean;
}

export class Home extends React.Component<{}, IHomeState> {

  public state = {
    scrollY: 0,
    isAboutVisible: true,
    isContactVisisble: false,
  }

  public componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, { passive: true });
  }

  public componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  public handleScroll = (event: Event) => {
    console.log('>>>>>>>>>', window.scrollY);
    const { scrollY } = this.state;
    const newScrollY = window.scrollY;

    if (scrollY > newScrollY) {
      this.setState((prevState) => ({
        ...prevState, scrollY: newScrollY, isAboutVisible: false, isContactVisisble: true
      }));
    } else {
      this.setState({scrollY: newScrollY, isAboutVisible: true, isContactVisisble: false});
    }
  }

  public render() {
    const { isAboutVisible, isContactVisisble } = this.state;

    return (
      <Container>
        Hello there....
        <div style={{ display: isContactVisisble ? 'block' : 'none' }}>
          <Contact />
        </div>

        <div style={{ display: isAboutVisible ? 'block' : 'none' }}>
          <About />
        </div>
      </Container>
    );
  }
}