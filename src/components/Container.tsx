/**
 * Author: bytesynk.com
 */

import * as React from 'react';
import '../App.css';

export class Container extends React.Component {
  public render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
